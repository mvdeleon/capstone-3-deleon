import {Form, Button, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate , Link  } from 'react-router-dom';
import Swal from 'sweetalert2';
import Register from './Register'


export default function Login() {


  const [email, setEmail] = useState ('');
  const [password, setPassword] = useState ('');
  const [isActive, setIsActive] = useState ('');

  console.log(email);
  console.log(password);

  const { user, setUser } = useContext(UserContext);


  function authenticate(e) {

      e.preventDefault()


      fetch('https://capstone2-online-shop-deleon.onrender.com/users/login', {
        method: 'POST',
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data);
        if(typeof data.access !== "undefined"){
          localStorage.setItem('token',data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: `Hi ${user}! Welcome back!`
          })
        }else{
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your credentials!"
          })
        }
      })


      const retrieveUserDetails = (token) =>{

        fetch('https://capstone2-online-shop-deleon.onrender.com/users/info', {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(res=>res.json())
        .then(data=>{
          console.log(data);

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        })
      }

      

      setEmail("");
      setPassword("");


  }

  useEffect(()=>{

    if(email !== "" && password !== ""){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
 

  }, [email, password])



  return (
 
    (user.id !== null)
    ?
    <Navigate to="/"/>
    :
    <Form onSubmit={(e)=>authenticate(e)} className="nav-shadow welcome-form p-5">
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter your email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password}
        onChange={e=>setPassword(e.target.value)}
        required
        />
      </Form.Group>
      
      { isActive ?
      <Button variant="success" type="submit" id="submitBtn">
        Login
      </Button>
      :
      <Button variant="danger" type="submit" id="submitBtn" disabled>
        Login
      </Button>
      }
      <div className="mt-5">
      <Row>
      <Col>
      Not a member yet?</Col>
      <Col><Button as={Link} to="/register">
        Sign Up Now!
      </Button></Col>
      </Row>
      </div>

    </Form>
  );
}
