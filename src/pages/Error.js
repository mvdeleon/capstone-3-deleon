import Banner from '../components/Banner.js'

export default function Error(){

	const data = {
		title: "404 - Not Found",
		content: "The page you are looking cannot be found.",
		destination: "/",
		label: "Home"

	}

	return (
		<Banner bannerProp = {data}/>
	)
}