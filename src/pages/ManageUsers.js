import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button, Table , Modal} from 'react-bootstrap';

export default function ManageUsers() {

  const {user} = useContext(UserContext);
  const navigate = useNavigate();
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [address, setAddress] = useState(['']);
  const [isActive, setIsActive] = useState(false);
  const [newPassword1, setNewPassword1] = useState('');
  const [newPassword2, setNewPassword2] = useState('');

	const [allUsers, setAllUsers] = useState([]);

// Name, Mobile
 const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

// Password
  const [show1, setShow1] = useState(false);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);


  function editPassword(e) {

      fetch(`https://capstone2-online-shop-deleon.onrender.com/users/changePassword`, {
        method: "PUT",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
          password: password,     
      })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if(data){
          Swal.fire({
              title: "Order succesfully Updated",
              icon: "success",
              text: ` is now updated`
          });

          navigate("/cart");
        }
        else{
          Swal.fire({
              title: "Error!",
              icon: "error",
              text: `Something went wrong. Please try again later!`
          });
        }

      })

  }

  useEffect(() => {

        if(password !== null && newPassword1 === newPassword2){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [password]);


// FETCH ORDER INFO
    useEffect(()=> {

      fetch(`https://capstone2-online-shop-deleon.onrender.com/users/all`,{
        headers:{
        'Content-Type':'application/json',
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
        setMobileNumber(data.mobileNumber);
        setAddress(data.address);


      });

    }, );

    const fetchData = () =>{
		
		fetch(`https://capstone2-online-shop-deleon.onrender.com/users/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return(
					<tr key={user._id}>
						<td className="white-text">{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.mobileNumber}</td>
						
						<td>
							{
								<>
								 <Button variant="info" onClick={handleShow}>
								        Update User Info
								      </Button>

								      <Button variant="light" onClick={handleShow1}>
								        Update Password
								      </Button>
								      </>
																
							}
						</td>
					</tr>
				)
			}))

		})
	}

useEffect(()=>{
		
		fetchData();
	})

    return (

      (user.isAdmin)
    ?

      <>
          <h3 className="my-5 white-text form-bg">Users Information</h3>
          <div className="right-align">
          <Button className="m-2" as={Link} to="/" variant="secondary" type="submit" id="submitBtn">
                    Home
        </Button></div><br/>
<div>
<Table>
<thead  className="gold form-bg">
  <th>First Name</th>
  <th>Last Name</th>
  <th>Email Address</th>
  <th>Mobile Number</th>
  <th>Update Info</th>
 
  <th></th>
</thead>
<tbody  className="white-text form-bg">
{allUsers}
</tbody>
</Table>
</div>



      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Info</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder={firstName}
                autoFocus
              />
            </Form.Group>
           
           <Form.Group className="mb-3" controlId="lastName">
            <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder={lastName}
                autoFocus
              />
            </Form.Group>
             
             <Form.Group className="mb-3" controlId="mobileNumber">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                type="text"
                placeholder={mobileNumber}
                autoFocus
              />
            </Form.Group>
             
             
                     
          
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Update
          </Button>
        </Modal.Footer>
      </Modal>

{/*PASSWORD*/}

      <Modal show={show1} onHide={handleClose1}>
        <Modal.Header closeButton>
          <Modal.Title>Update Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={(e)=>editPassword(e)} className="slide-text p-5">
            <Form.Group className="mb-3" controlId="oldPassword">
              <Form.Label>Old Password</Form.Label>
              <Form.Control
                type="text"
                placeholder="Your current password"
                autoFocus
              />
            </Form.Group>
           
           <Form.Group className="mb-3" controlId="newPassword1">
            <Form.Label>New Password</Form.Label>
              <Form.Control
                type="text"
                placeholder="New Password"
                autoFocus
              />
            </Form.Group>
             
             <Form.Group className="mb-3" controlId="newPassword2">
              <Form.Label>Retype New Password</Form.Label>
              <Form.Control
                type="text"
                placeholder="Re-type new password"
                autoFocus
              />
            </Form.Group>
             
        { isActive 
                  ?      
           <Button variant="primary" variant="primary" type="submit" id="submitBtn">
            Update
          </Button>           
          :
           <Button variant="danger" type="submit" id="submitBtn" disabled>
                      Update
                    </Button>
                }
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose1}>
            Close
          </Button>
         
        </Modal.Footer>
      </Modal>
            
        </>
        :
    <Navigate to="/allproducts/" />

    )

}