import { Form , Row , Col, Table} from 'react-bootstrap';


export default function ContactUs(){

		
return(
	<>
	    	<h1 className="my-5 text-center gold">Contact Us</h1>
		        <Form className="slide-text m-5 p-3">
		        	<Form.Group controlId="name" className="mb-3">
		                <Form.Label>First and Last Name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="First and Last Name" 
			                value = " "/>
		            </Form.Group>

		      
		        	<Form.Group controlId="name" className="mb-3">
		                <Form.Label>Email Address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="sample@mail.com" 
			                />
		            </Form.Group>
		            

		            <Form.Group controlId="description" className="mb-3">
		                <Form.Label>Inquiry</Form.Label>
		                <Form.Control
		                	as="textarea"
		                	rows={5}
			                placeholder="09XX-XXX-XXXX" 
			                />
		            </Form.Group>
		            	<button className="button-highlight">Send</button>
		        </Form>


		        <Row>
		<Col className="right-align white-text col-6 mt-5 p-4"><hr/>
			<Table>
				<thead className="gold form-bg">
				<th>Business Address:</th></thead>
				<tbody className="white-text center form-bg">
				<tr>123 Main St,</tr> 
				<tr>Antipolo City </tr>
				<tr>Philippines </tr>
				<tr>1800</tr>
				</tbody>
			</Table>
				</Col>
		<Col className="right-align white-text col-6 mt-5 p-4"><hr/>

			<Table>
				<thead className="gold form-bg">
				<th>Contact Us:</th></thead>
				<tbody className="white-text center form-bg">
				<tr>Toll Free: 800-123-1234</tr> 
				<tr>Mobile Number: 0916-123-1234</tr>
				<tr>Email: admin@mail.com</tr>
				<tr>Social Media | Facebook | Instagram | Twitter | LinkedIn</tr>
				</tbody>
			</Table>


		
				</Col>
		</Row>
		        </>
	    	
	)
}
