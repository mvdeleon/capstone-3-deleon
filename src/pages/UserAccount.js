import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button, Table , Modal} from 'react-bootstrap';

export default function UserAccount() {

  const {user} = useContext(UserContext);
  const navigate = useNavigate();
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [address, setAddress] = useState(['']);
  const [isActive, setIsActive] = useState(false);
  const [newPassword1, setNewPassword1] = useState('');
  const [newPassword2, setNewPassword2] = useState('');

  const { unsetUser, setUser } = useContext(UserContext);


   const [firstName1, setFirstName1] = useState('');
  const [lastName1, setLastName1] = useState('');
  const [email1, setEmail1] = useState('');
  const [mobileNumber1, setMobileNumber1] = useState('');
  const [address1, setAddress1] = useState(['']);

// Name, Mobile
 const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

// Password
  const [show1, setShow1] = useState(false);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);


  function editPassword(e) {

    e.preventDefault(e);

      fetch(`https://capstone2-online-shop-deleon.onrender.com/users/changePassword`, {
        method: "PUT",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
          password: password,     
      })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if(data){
          Swal.fire({
              title: "Password succesfully Updated",
              icon: "success",
              text: `Please login in with your new password.`
          });

        }
        else{
          Swal.fire({
              title: "Error!",
              icon: "error",
              text: `Something went wrong. Please try again later!`
          });
        }

      })

  }

  useEffect(() => {

        if(password !== null && newPassword1 === newPassword2){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [password]);



 function editInfo(e) {

    e.preventDefault(e);

      fetch(`https://capstone2-online-shop-deleon.onrender.com/users/updateUser`, {
        method: "PUT",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          mobileNumber: mobileNumber,
          address: address

      })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if(data){
          Swal.fire({
              title: "Info succesfully Updated",
              icon: "success",
              text: `:)`
          });

        }
        else{
          Swal.fire({
              title: "Error!",
              icon: "error",
              text: `Something went wrong. Please try again later!`
          });
        }

      })

        setFirstName('');
  setLastName('');
  setMobileNumber('');
  setAddress('');

  }





  useEffect(() => {

        if(password !== null && newPassword1 === newPassword2){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [password]);


// FETCH ORDER INFO
    useEffect(()=> {

      fetch(`https://capstone2-online-shop-deleon.onrender.com/users/info`,{
        headers:{
        'Content-Type':'application/json',
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        setFirstName1(data.firstName);
        setLastName1(data.lastName);
        setEmail1(data.email);
        setMobileNumber1(data.mobileNumber);
        setAddress1(data.address);


      });

    }, );

    return (

        (user.isAdmin===false)
                ?
      <>
          <h3 className="my-5 white-text form-bg">Account Information</h3>
          <div className="right-align">
          <Button className="m-2" as={Link} to="/" variant="secondary" type="submit" id="submitBtn">
                    Home
        </Button></div><br/>
<div>
<Table>
<thead  className="white-text form-bg">
  <th>First Name</th>
  <th>Last Name</th>
  <th>Email Address</th>
  <th>Mobile Number</th>
  <th>Shipping Address</th>
  <th>Update Info</th>
 
  <th></th>
</thead>
<tbody  className="white-text form-bg">
<tr>
<td>{firstName1}</td>
<td>{lastName1}</td>
<td>{email1}</td>
<td>{mobileNumber1}</td>
<td>{address1}</td>
<td>
     
      <Button variant="info" onClick={handleShow}>
        Update User Info
      </Button>

      <Button variant="light" onClick={handleShow1}>
        Update Password
      </Button>

</td>

</tr>
</tbody>
</Table>
</div>



      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Info</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={(e)=>editInfo(e)} className="slide-text p-5">
            <Form.Group className="mb-3" controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control 
                      type="text" 
                      placeholder={firstName} 
                      value={firstName}
                      onChange={e => setFirstName(e.target.value)}
                    />
              
            </Form.Group>
           
           <Form.Group className="mb-3" controlId="lastName">
            <Form.Label>Last Name</Form.Label>
              <Form.Control 
                      type="text" 
                      placeholder={lastName} 
                      value={lastName}
                      onChange={e => setLastName(e.target.value)}
                    />
            </Form.Group>
             
             <Form.Group className="mb-3" controlId="mobileNumber">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control 
                      type="text" 
                      placeholder={mobileNumber} 
                     value={mobileNumber}
                      onChange={e => setMobileNumber(e.target.value)}
                    />
            </Form.Group>

             <Form.Group className="mb-3" controlId="address">
              <Form.Label>Shipping setAddress</Form.Label>
              <Form.Control 
                      type="text" 
                      placeholder={address} 
                      value={address}
                      onChange={e => setAddress(e.target.value)}
                      required
                    />
            </Form.Group>
             
             <Button variant="primary" type="submit" id="submitBtn" onClick={handleClose}>
            Update
          </Button>
                     
          
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          
        </Modal.Footer>
      </Modal>

{/*PASSWORD*/}

      <Modal show={show1} onHide={handleClose1}>
        <Modal.Header closeButton>
          <Modal.Title>Update Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={(e)=>editPassword(e)} className="slide-text p-5">
            <Form.Group className="mb-3" controlId="oldPassword">
              <Form.Label>Old Password</Form.Label>
              <Form.Control
                type="text"
                placeholder="Your current password"
                autoFocus
              />
            </Form.Group>
           
           <Form.Group className="mb-3" controlId="newPassword1">
            <Form.Label>New Password</Form.Label>
              <Form.Control
                type="text"
                placeholder="New Password"
                autoFocus
              />
            </Form.Group>
             
             <Form.Group className="mb-3" controlId="newPassword2">
              <Form.Label>Retype New Password</Form.Label>
              <Form.Control
                type="text"
                placeholder="Re-type new password"
                autoFocus
              />
            </Form.Group>
             
        { isActive 
                  ?      
           <Button variant="primary" variant="primary" type="submit" id="submitBtn">
            Update
          </Button>           
          :
           <Button variant="danger" type="submit" id="submitBtn" disabled>
                      Update
                    </Button>
                }
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose1}>
            Close
          </Button>
         
        </Modal.Footer>
      </Modal>
            
        </>

         :
    <Navigate to="/"/>

    )

}