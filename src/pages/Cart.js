import { useContext, useState, useEffect } from "react";
import {Table, Button, Row, Col} from "react-bootstrap";
import {Navigate, Link, useParams, useNavigate} from "react-router-dom";
import UserContext from "../UserContext";
	
import Swal from "sweetalert2";

export default function Cart(){

	
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [allOrders, setAllOrders] = useState([]);




	const fetchData = () =>{
		
		fetch(`https://capstone2-online-shop-deleon.onrender.com/orders/checkOrder/`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(order => {
				return(
					<tr key={order._id}>
						<td className="white-text">{order._id}</td>
						<td>{order.name}</td>					
						<td>{order.price}</td>
						<td>{order.quantity}</td>
						<td>{order.quantity * order.price}</td>
						<td>
							<Button as={ Link } to={`/editcart/${order._id}`} variant="secondary" size="sm" className="m-2" >Edit/Delete</Button>
							<Button as={ Link } to={`/checkout/${order._id}`} variant="success" size="sm" className="m-2" >CheckOut</Button>
							
						</td>
						
					</tr>
				)
			}))

		})
	}

	useEffect(()=>{
		
		fetchData();
	})




	return(
		
		(user.isAdmin===false)
								?
								
		<>
		

			<div className="mt-5 mb-3 text-center white-text">
				<h3>Shopping Cart</h3>				
				
			</div>
			<div className="mt-5 mb-3 right-align white-text">
				<Button variant="success" as={Link} to={`/allproducts`}>Add more items</Button>
				<Button variant="info" as={Link} to={`/showorder`}>Order History</Button>		
				
			</div>	

			<Table className="nav-shadow white-text">
		     <thead>
		       <tr className="slide-text">
		         <th>Order ID</th>
		         <th>Product Name</th>		         
		         <th>Price</th>
		          <th>Quantity</th>
		          <th>Sub-total</th>
		          <th>Edit/Delete</th>

		       </tr>
		     </thead>
		     <tbody className="slide-text">
		       { allOrders }
		     </tbody>
		   </Table>
	
		</>
		:
		<Navigate to="/"/>
		
	)
}