

import Products from './Products';
import { Row, Col } from 'react-bootstrap';
import { useContext } from 'react';
import UserContext from '../UserContext';
import ProdCat1 from '../components/ProdCat1';
import ProdCat2 from '../components/ProdCat2';
import ProdCat3 from '../components/ProdCat3';


export default function AllProducts(){


	const { user } = useContext(UserContext);

	return(

		<>
		
		

				
				<Row>
				<Col className="col-4 px-5">
				<ProdCat1/>
				</Col>
				
				<Col className="col-4 px-5">
				<ProdCat2/>
				</Col>

				<Col className="col-4 px-5">
				<ProdCat3/>
				</Col>

				
				</Row>
				
				
				
			
		</>

	)
}
