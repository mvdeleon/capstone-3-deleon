import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button, Table } from 'react-bootstrap';

export default function CheckOut() {

  const {user} = useContext(UserContext);
  const { orderId } = useParams();
  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [productId, setProductId] = useState('');
  const [address, setAddress] = useState('');
  const [quantity, setQuantity] = useState(0);
  const [price, setPrice] = useState(0);
  const [totalAmount, setTotalAmount] = useState(0);



 	const checkOut = (e) =>{


		fetch(`https://capstone2-online-shop-deleon.onrender.com/orders/checkout/${orderId}`,{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({				
				orderId: orderId,
							
				
			})
		})
		.then(res=>res.text())
		.then(data=>{
			
      if(data){
          Swal.fire({
              title: "Order has been successfully placed.",
              icon: "success",
              text: `Delivery is within 7-14 days. Please prepare the amount of ${totalAmount}. Please make sure to add/update your address to avoid delay.`
          });

        }
        else{
          Swal.fire({
              title: "Error!",
              icon: "error",
              text: `Something went wrong. Please try again later!`
          });
        }

			navigate("/cart")

		})
	}

	useEffect(()=>{

		console.log(productId);
		fetch(`https://capstone2-online-shop-deleon.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setPrice(data.price);
			setQuantity(data.quantity);
			
					
			
		})

	},[productId])


// FETCH ORDER INFO
    useEffect(()=> {

      console.log(orderId);

      fetch(`https://capstone2-online-shop-deleon.onrender.com/orders/checkOrder/${orderId}`,{
        headers:{
        'Content-Type':'application/json',
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        setName(data.name);
        setProductId(data.productId);
        setPrice(data.price);
        setAddress(data.address);
        setQuantity(data.quantity);
        setTotalAmount(data.totalAmount);
        

      });

    }, [orderId]);

    return (

        (user.isAdmin===false)
                ?

      <>
          <h3 className="my-5 white-text form-bg">Check Out</h3>
          <div className="right-align">
          <Button className="m-2" as={Link} to="/cart" variant="secondary" type="submit" id="submitBtn">
                    Back to Cart
        </Button></div><br/>
<div>
<Table>
<thead  className="white-text form-bg">
  <th>Product ID</th>
  <th>Product Name</th>
  <th>Price</th>
  <th>Quantity</th>
  <th>Total</th>
  <th></th>
  <th></th>
</thead>
<tbody  className="white-text form-bg">
<tr>
<td>{productId}</td>
<td>{name}</td>
<td>{price}</td>
<td>{quantity}</td>
<td>{totalAmount}</td>
<td>
     
      <Button variant="primary" type="submit" id="submitBtn" onClick={()=>checkOut(orderId)}>
      CheckOut
      </Button></td>
       
    <td>  
 
</td>
<td className="gold"><Link to={`/products/${productId}`}> See Product Details</Link></td>
</tr>
<tr>
<td>Shipping Address: {address}</td>
</tr>
</tbody>
</Table>
</div>

            
        </>

          :
    <Navigate to="/"/>

    )

}