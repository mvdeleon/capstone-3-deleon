import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Form, Button } from 'react-bootstrap';


export default function Register() {

  const {user} = useContext(UserContext);

  const navigate = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email,setEmail] = useState('');
    const [mobileNumber, setmobileNumber] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNumber);
    console.log(password1);
    console.log(password2);

  function registerUser(e) {

    
      e.preventDefault();

        fetch(`https://capstone2-online-shop-deleon.onrender.com/users/check`,{
        method: "POST",
        headers:{
            'Content-Type':'application/json'
          },
        body: JSON.stringify({
          email: email
                })
                })
        .then(res=>res.json())
        .then(data=>{
          console.log(data)

          if(data){

            Swal.fire({
            title: "Duplicate Email Found!",
            icon: "error",
            text: "Kindly provide another email to complete the registration!"
          })
          

          }else(
        fetch(`https://capstone2-online-shop-deleon.onrender.com/users/signup`, {
        method: "POST",
        headers: {
        "Content-Type": "application/json",
        
        
                },
                body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNumber: mobileNumber,
                        password: password1
                })
                })
          .then(res=>res.json())
          .then(data=>{
            console.log(data);

        if(data){
          Swal.fire({
            title: "Registration successful!",
                icon: "success",
                text: "You are now registered.  !"
          });

          navigate("/");
        }
        else{
          Swal.fire({
              title: "Error!",
              icon: "error",
              text: `Something went wrong. Please try again later!`
          });
        } 
        

          })

        )

      })

  }

  useEffect(() => {

    if((firstName !=="" && lastName !== "" && mobileNumber.length === 11 && email !=="" && password1 !=="" && password2 !=="")&&(password1===password2)){
      setIsActive(true)
    }else{
      setIsActive(false)
    }

  },[firstName,lastName,email,mobileNumber,password1,password2])

    return (
      (user.id !== null)
    ?
    <Navigate to="/"/>
  
    :
    <Form onSubmit={(e)=>registerUser(e)} className="m-5">

      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter First Name"
        value={firstName}
        onChange={e=>setFirstName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter Last Name"
        value={lastName}
        onChange={e=>setLastName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="email">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required
        />
 
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter Mobile Number"
        value={mobileNumber}
        onChange={e=>setmobileNumber(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password1}
        onChange={e=>setPassword1(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Verify Password"
        value={password2}
        onChange={e=>setPassword2(e.target.value)}
        required/>
      </Form.Group>

      <div className="right-align">
        { isActive ?      
            <Button variant="success" type="submit" id="submitBtn">
                Submit
            </Button>
          :
            <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
            </Button>
        }
        </div>
    </Form>
        
    )

}