import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import NavLower from './components/NavLower';
import Home from './pages/Home';
import Products from './pages/Products';
import UserAccount from './pages/UserAccount';
import ProductView from './components/ProductView';
import Register from './pages/Register';
import CheckOut from './pages/CheckOut';
import Logout from './pages/Logout';
import Error from './pages/Error';
import ShowOrder from './components/ShowOrder';
import Cart from './pages/Cart';
import EditCart from './components/EditCart';
import Dash from './components/Dash';
// import Featured from './components/Featured';
import AddProducts from './components/AddProducts';
import ProdCat1 from './components/ProdCat1';
import EditProduct from './components/EditProduct';
import AllOrders from './components/AllOrders';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import './App.css';
import ContactUs from './pages/ContactUs';
import AboutUs from './pages/AboutUs';
import ManageUsers from './pages/ManageUsers';
import AllProducts from './pages/AllProducts';



function App() {


  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

 
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=>{
    fetch('http://localhost:4000/users/info',{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[]);

  

  return (
    

    <UserProvider value = {{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <NavLower/>
      
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/admin" element={<Dash/>}/>
              <Route path="/useraccount" element={<UserAccount/>}/>
              <Route path="/manageusers" element={<ManageUsers/>}/>
              <Route path="/aboutus" element={<AboutUs/>}/>
              <Route path="/addproducts" element={<AddProducts/>} />
              <Route path="/editproduct/:productId" element={<EditProduct/>} />
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/:productId" element={<ProductView/>}/>
              <Route path="/cart" element={<Cart/>}/>
              <Route path="/showorder" element={<ShowOrder/>}/>
              <Route path="/checkout/:orderId" element={<CheckOut/>}/>
              <Route path="/editcart/:orderId" element={<EditCart/>}/>      
              <Route path="/allorders" element={<AllOrders/>}/> 
              <Route path="/allproducts" element={<AllProducts/>}/>      
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>}/>   
              <Route path="/contactus" element={<ContactUs/>}/>             
              <Route path="*" element={<Error/>}/>
            </Routes>
          </Container>
         
        </Router>
    </UserProvider>


       

  );
}

export default App;
