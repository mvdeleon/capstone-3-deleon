import {Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({bannerProp}){

	let {title,content,destination,label} = bannerProp;
	return (
		<div className="text-center my-5">
			<Row>
				<Col>
					<h1 className="red">{title}</h1>
					<p className="red">{content}</p>
					<Button as={Link} to={destination} variant="danger">{label}</Button>
				</Col>
			</Row>
		</div>

	)
}