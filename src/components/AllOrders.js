import { useContext, useState, useEffect } from "react";
import {Table, Button, Row, Col} from "react-bootstrap";
import {Navigate, Link, useParams, useNavigate} from "react-router-dom";
import UserContext from "../UserContext";
	
import Swal from "sweetalert2";

export default function ShowOrder(){

	
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [allOrders, setAllOrders] = useState([]);




	const fetchData = () =>{
		
		fetch(`https://capstone2-online-shop-deleon.onrender.com/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product.name}</td>	
							
						<td>{product.orders.map(order =>{
							return(
								<span key={order._id}><span className="gold">UserId:</span> {order.userId} | <span className="gold">Quantity:</span> {order.quantity} | <span className="gold">Date of Purchase:</span> {order.purchasedOn}<hr/></span>
								)
						})}</td>	

					
					</tr>
				)
			}))

		})
	}

	useEffect(()=>{
		
		fetchData();
	})




	return(
		
		<>
		

			<div className="mt-5 mb-3 text-center white-text">
				<h3>All Completed Orders</h3>				
				
			</div>
			<div className="mt-5 mb-3 right-align white-text">
				<Button variant="success" as={Link} to={`/admin`}>Back to Manage Products</Button>
						
				
			</div>	

			<Table className="nav-shadow white-text">
		     <thead>
		       <tr className="slide-text nav-shadow">
		         
		         <th>Product Name</th>		         
		         <th>Orders</th>
		         


		       </tr>
		     </thead>
		     <tbody className="slide-text">
		       { allOrders }
		     </tbody>
		   </Table>
	
		</>
		
	)
}