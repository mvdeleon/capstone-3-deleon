import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import {Row , Col} from 'react-bootstrap';


export default function ProdCat2(){


	const [product, setProducts] =useState([])

	 const [isActive, setIsActive] = useState(false);

	useEffect(()=>{
		
	fetch('https://capstone2-online-shop-deleon.onrender.com/products/group/Vodka')
	.then(res=>res.json())
	.then(data=>{

	const productArr = (data.map(product => {
		
		return (

			<ProductCard productProp={product} key={product._id}/>
			)
		}))

		setProducts(productArr)
	})

	},[product])


	return(
		<>
			<Row className="d-flex">
			<Col>
			{product}
			</Col>
			</Row>
		</>
	)
}