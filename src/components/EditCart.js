import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button, Table } from 'react-bootstrap';

export default function EditCart() {

  const {user} = useContext(UserContext);
  const { orderId } = useParams();
  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [productId, setProductId] = useState('');
  const [quantity, setQuantity] = useState(0);
  const [price, setPrice] = useState(0);
  const [isActive, setIsActive] = useState(false);


  function editCart(e) {

      fetch(`https://capstone2-online-shop-deleon.onrender.com/orders/${orderId}`, {
        method: "PUT",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
          quantity: quantity,     
      })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if(data){
          Swal.fire({
              title: "Order succesfully Updated",
              icon: "success",
              text: `${name} is now updated`
          });

          navigate("/cart");
        }
        else{
          Swal.fire({
              title: "Error!",
              icon: "error",
              text: `Something went wrong. Please try again later!`
          });
        }

      })

  }

  useEffect(() => {

        if(quantity > 0 ){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [quantity]);



// EXECUTE DELETE

  const removeItem = (orderId) => {

      fetch(`https://capstone2-online-shop-deleon.onrender.com/orders/remove/${orderId}`, {
        method: "DELETE",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      
      })
      .then(() => {

        navigate("/cart");
      })
          
          
        

      

}

// FETCH ORDER INFO
    useEffect(()=> {

      console.log(orderId);

      fetch(`https://capstone2-online-shop-deleon.onrender.com/orders/checkOrder/${orderId}`,{
        headers:{
        'Content-Type':'application/json',
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        setName(data.name);
        setProductId(data.productId);
        setPrice(data.price);
        setQuantity(data.quantity);

      });

    }, [orderId]);

    return (

      <>
          <h3 className="my-5 white-text form-bg">Update Order</h3>
          <div className="right-align">
          <Button className="m-2" as={Link} to="/cart" variant="secondary" type="submit" id="submitBtn">
                    Back to Cart
        </Button></div><br/>
<div>
<Table>
<thead  className="white-text form-bg">
  <th>Product ID</th>
  <th>Product Name</th>
  <th>Price</th>
  <th>Quantity</th>
  <th>Edit/Delete</th>
  <th></th>
</thead>
<tbody  className="white-text form-bg">
<tr>
<td>{productId}</td>
<td>{name}</td>
<td>{price}</td>
<td><input type="number" name="counter" className="counter" value={quantity} onChange={e=>setQuantity(e.target.value)}/></td>
<td>
     { isActive 
      ? 
      <Button variant="primary" type="submit" id="submitBtn" onClick={()=>editCart(quantity)}>
      Update
      </Button>
                    : 
      <Button variant="danger" type="submit" id="submitBtn" disabled>
                      Update
                    </Button>
      }
      <Button variant="danger" type="submit" id="submitBtn" onClick={()=>removeItem(orderId)}>
                    Delete
        </Button>
</td>
<td className="gold"><Link to={`/products/${productId}`}> See Product Details</Link></td>
</tr>
</tbody>
</Table>
</div>

            
        </>

    )

}