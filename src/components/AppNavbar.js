import { useState, useEffect, useContext } from 'react';
import {Navbar, Nav, Container, Modal , Button, Form , Row , Col} from 'react-bootstrap';
import { Link , Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function AppNavbar(){

	const {user, setUser} = useContext(UserContext);
	const [firstName, setFirstName] = useState('');

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

  const [email, setEmail] = useState ('');
  const [password, setPassword] = useState ('');
  const [isActive, setIsActive] = useState ('');

  console.log(email);
  console.log(password);

  function authenticate(e) {

      e.preventDefault()


      fetch('https://capstone2-online-shop-deleon.onrender.com/users/login', {
        method: 'POST',
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data);
        if(typeof data.access !== "undefined"){
          localStorage.setItem('token',data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: `Hi! Welcome back!`
          })
        }else{
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your credentials!"
          })
        }
      })


      const retrieveUserDetails = (token) =>{

        fetch('https://capstone2-online-shop-deleon.onrender.com/users/info', {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(res=>res.json())
        .then(data=>{
          console.log(data);

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        })
      }

      

      setEmail("");
      setPassword("");


  }

  useEffect(()=>{

    if(email !== "" && password !== ""){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
 

  }, [email, password])

// GET FIRST NAME

	   useEffect(()=> {

      fetch(`https://capstone2-online-shop-deleon.onrender.com/users/info`,{
        headers:{
        'Content-Type':'application/json',
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        setFirstName(data.firstName);
 


      });

    }, );

	
	

	return(
		<Navbar variant="dark" expand="lg" className="slide-text" fixed="top">
	      <Container fluid className="slide-text">
	      <Nav>
	        <Navbar.Brand as={Link} to="/" className="slide-text">
	       <h1 className="h1"> Katzen </h1>
	        </Navbar.Brand></Nav><h7>Let's Celebrate Your Success!</h7>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">          
	        </Navbar.Collapse>
	        
	            
	        <Nav>


	            {(user.id !== null) ?
	            	
	            	<>
	            	<Navbar.Text className="mx-2">
	            	{(user.isAdmin)?
							           <span>Hi, {firstName}! [admin] | <Link to="/admin" className="px-2">Manage Products</Link> | <Link to="/manageusers">Manage Users</Link></span>
							            :
							            <span>Hi, {firstName}! [user] | <Link to="/useraccount">My Account</Link> | <Link to="/cart">My Orders</Link></span>
							        }
							          </Navbar.Text>
						<Nav.Link as={Link} to="/logout">	          <button className="slide-text">Logout</button>
	            	</Nav.Link></>
	            	:
	            	<>
	            		
	            		      <Button variant="warning" onClick={handleShow} className="mx-2 mt-2 mb-2">  Log In      </Button> 
	            		<Nav.Link as={Link} to="/register"><button className="slide-text">Register</button></Nav.Link>
	            	</>

	            }
	          </Nav>
	           <>
	          <Modal show={show} onHide={handleClose} className="welcome-form">
        <Modal.Header closeButton >
          <Modal.Title>Sign In</Modal.Title>
        </Modal.Header>
        <Modal.Body className="welcome-form padding-nav-lower">

             
    <Form onSubmit={(e)=>authenticate(e)} className="slide-text p-5">
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter your email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password}
        onChange={e=>setPassword(e.target.value)}
        required
        />
      </Form.Group>
      
      { isActive ?
      <Button variant="warning" type="submit" id="submitBtn" onClick={handleClose}>
        Login
      </Button>
      :
      <Button variant="danger" type="submit" id="submitBtn" disabled>
        Login
      </Button>
      }
      <div className="mt-5">
      <Row>
      <Col>
      Not a member yet?</Col>
      <Col><Button variant="info" as={Link} to="/register">
        Sign Up Now!
      </Button></Col>
      </Row>
      </div>

    </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          
        </Modal.Footer>
      </Modal>
    </>
	      </Container>
	    </Navbar>

	)
}