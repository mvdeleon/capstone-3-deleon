import { Link } from 'react-router-dom';
import { Button, Row, Col, Table } from 'react-bootstrap';


export default function ProductCard({productProp}){

	let { name, price, category, photo, _id } = productProp;

	return(

		<div className="padding-nav">
		<Row className="mx-1">
		

		<img src={photo} className="d-block"/>

		

		

		<Table className="nav-shadow">
		<thead>
			<th className="product-title">{name}</th>
		</thead>
		<tbody className="white-text">
		<tr>
		
		<td className="white-text">Category: {category}</td></tr>
		
		
		
		<tr><td className="white-text">Price:  PHP {price}</td>
		<td > </td></tr>
		<td className="right-align"><Button variant="success" as={Link} to={`/products/${_id}`}>More Info</Button></td>
		

		
		</tbody>


		</Table>

		
		</Row>
		</div>
		

		

		
	

	



	)
}