import { useState, useContext } from 'react';
import {Navbar, Nav, Container, Form} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function NavLower(){

	const { user } = useContext(UserContext);

	return(
		<Navbar>
	      <Container fluid className="padding-nav-lower">

	        
	          <Nav className="mx-5">
	            <Nav.Link as={Link} to="/"><button className="nav-top">Home</button></Nav.Link>
	            <Nav.Link as={Link} to="/allproducts"><button className="nav-top">Products</button></Nav.Link>
	            <Nav.Link as={Link} to="/aboutus"><button className="nav-top">About Us</button></Nav.Link>
	            <Nav.Link as={Link} to="/contactus"><button className="nav-top">Contact Us</button></Nav.Link>
	            

	            
	          </Nav>
	        
	      </Container>
	    </Navbar>
	)
}