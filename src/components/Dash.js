import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Dash(){

	
	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () =>{
		
		fetch(`https://capstone2-online-shop-deleon.onrender.com/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td className="white-text">{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td><img src={product.photo} className="d-block w-30"/></td>
						<td>{product.category}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{(product.isActive)
								?	
									<Button variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>Archive</Button>
									
								:
									<>
										
										<Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
										
										<Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>
									</>
							}
							</td>
							<td>
							{(product.isFeatured)
								?	
								 	
									<Button variant="secondary" size="sm" onClick ={() => unfeature(product._id, product.name)}>Unfeature</Button>
								:
									
										
										<Button variant="info" size="sm" onClick ={() => feature(product._id, product.name)}>Feature</Button>
			
									
							}
						</td>

							
					</tr>
				)
			}))

		})
	}

	
	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://capstone2-online-shop-deleon.onrender.com/products/archive/${productId}/`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			fetchData();

			
		})
	}

	//Making the product active
	const unarchive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://capstone2-online-shop-deleon.onrender.com/products/activate/${productId}/`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	};


		const feature = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://capstone2-online-shop-deleon.onrender.com/products/feature/${productId}/`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isFeatured: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "This is now featured!",
					icon: "success",
					text: `${productName} is now featured.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Featuring Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

const unfeature = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://capstone2-online-shop-deleon.onrender.com/products/unfeature/${productId}/`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isFeatured: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unfeature Succesful!",
					icon: "success",
					text: `${productName} is now removed.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Not removed!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}









	useEffect(()=>{
		
		fetchData();
	})
	

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center white-text">
				<h1>Manage Products</h1>
				
				
			</div>
			<div className="right-align mb-3 white-text">
			<Button as={Link} to="/addProducts" variant="info" size="lg" className="mx-2">Add Product</Button>
			<Button as={Link} to="/allorders" variant="success" size="lg" className="mx-2">Show Orders</Button>
			
			</div>

			<Table className="nav-shadow white-text">
		     <thead>
		       <tr className="slide-text">
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Photo</th>
		         <th>Category</th>
		         <th>Status</th>
		         <th>Action</th>
		         <th>Feature</th>
		       </tr>
		     </thead>
		     <tbody className="slide-text">
		       { allProducts }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/allproducts/" />
	)
}