import { useContext, useState, useEffect } from "react";
import {Table, Button, Row, Col} from "react-bootstrap";
import {Navigate, Link, useParams, useNavigate} from "react-router-dom";
import UserContext from "../UserContext";
	
import Swal from "sweetalert2";

export default function ShowOrder(){

	
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [allOrders, setAllOrders] = useState([]);




	const fetchData = () =>{
		
		fetch(`https://capstone2-online-shop-deleon.onrender.com/users/showCart`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(user => {
				return(
					<tr key={user._id}>
						<td className="white-text ">{user._id}</td>
						<td>{user.name}</td>	
						<td>{user.totalAmount}</td>				
						<td>{user.purchasedOn}</td>
						{/*<td>{user.products}</td>*/}
						
						<td>{user.isPaid}</td>
					</tr>
				)
			}))

		})
	}

	useEffect(()=>{
		
		fetchData();
	})




	return(
		(user.isAdmin===false)
								?
								
		<>
		

			<div className="mt-5 mb-3 text-center white-text">
				<h3>Shopping Cart</h3>				
				
			</div>
			<div className="mt-5 mb-3 right-align white-text">
				<Button variant="success" as={Link} to={`/cart`}>Back to Cart</Button>
						
				
			</div>	

			<Table className="nav-shadow white-text">
		     <thead>
		       <tr className="slide-text nav-shadow">
		         <th>Order ID</th>
		         <th>Product Name</th>		         
		         <th>Total Paid</th>
		         <th>Date Purchased</th>


		       </tr>
		     </thead>
		     <tbody className="slide-text">
		       { allOrders }
		     </tbody>
		   </Table>
	
		</>
		:
		<Navigate to="/"/>
	)
}