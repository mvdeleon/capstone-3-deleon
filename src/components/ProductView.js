import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col , Table} from 'react-bootstrap';
import { useParams, useNavigate, Link, Navigate  } from 'react-router-dom';
import UserContext from '../UserContext';



export default function ProductView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");	
	const [photo, setPhoto] = useState("");	
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const [orderProdId, setOrderProdId] = useState(false);


	
	const addToCart = (e) =>{


		fetch('https://capstone2-online-shop-deleon.onrender.com/users/createOrder',{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				
				productId: productId,
				quantity: quantity				
				
			})
		})
		.then(res=>res.text())
		.then(data=>{



			console.log("success")
			navigate("/cart")

		})
	}

	useEffect(()=>{

		console.log(productId);
		fetch(`https://capstone2-online-shop-deleon.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setPhoto(data.photo);
		
			
		})

	},[productId])

	// GET PRODUCT ID (ORDER MATCH)

	   



	return(
		

		<Container className="mt-1 noborder">
		  <Row>
		   <Col className="col-3">
			<img src={photo} alt={name}/>
		   </Col>
		     <Col className="col-6 ">
				<Card className="white-text noborder">
					<Card.Body className="gray-bg ">
						<Card.Title><h3 className="product-title">{name}</h3></Card.Title>
						<Card.Subtitle><h5>Description:</h5></Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PHP {price}</Card.Text>
						
					</Card.Body>
				</Card>
		     </Col>
		     
		      <Col className="m-3">
				
		      <Table className="nav-shadow mx-3 p-3 slide-text ">
		      <thead></thead>
		      <tbody className="m-3 white-text noborder">
						<tr>
						<td>Product:</td> <td>{name}</td></tr>
						<tr>
						<td>Price:</td> <td>PHP {price}</td></tr>
						
					           
                
             <tr><td>Quantity: </td><input type="number" name="counter" className="counter border white-text" value={quantity} onChange={e=>setQuantity(e.target.value)}/>
                          	

                          	

             </tr>

              
						
    					 <tr><td>Sub-Total:</td> <td>PHP {price * quantity}</td></tr>

						{
							
							(user.id !== null)?

							(user.isAdmin===false)
								?
							
								<>
								<tr><td></td><td>
								
								<Button variant="success" onClick={()=>addToCart(productId,quantity)}>Add to Cart</Button>


								</td></tr>

								</>
								

								:
								<tr className="red"><td></td><td>Log in as USER to Purchase</td></tr>
								
								:
																		
								<tr className="red right-align"><td></td><td>Log in to Purchase</td></tr>
								
											
						}

						<tr><td></td><td className="right-align"><Button variant="secondary" as={Link} to={`/allproducts/`}>Back</Button></td></tr>
					</tbody>
					</Table>
		     </Col>

		  </Row>
		</Container>
	)
}