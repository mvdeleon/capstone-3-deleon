import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Featured() {

  
  const {user} = useContext(UserContext);

  const [allProducts, setAllProducts] = useState([]);

const fetchData = () =>{
    
    fetch(`https://capstone2-online-shop-deleon.onrender.com/products/featured`,{
      headers:{
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setAllProducts(data.map(product => {
        return(
          <tr key={product._id}>
            <td><img src={product.photo} className="d-block"/></td>
            <td><h5>{product.name}</h5></td>
            <td className="center">{product.description}</td>
            <td><Button as={Link} to="/allproducts" variant="warning" size="lg" className="mx-2">Purchase now!</Button></td>
            
          
          </tr>
        )
      }))

    })
  }

  useEffect(()=>{
    
    fetchData();
  })

  return (
    
   
    <>
      <h1 className="form-bg gold pt-5 mt-5 mb-5"> Featured Products </h1>
      <Table className="nav-shadow white-text">
         <thead>
           <tr className="slide-text">
             
             

           </tr>
         </thead>
         <tbody className="slide-text">
           { allProducts }
         </tbody>
       </Table>
    </>
   
  );
}

