import { Col, Row , Table} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Footer(){

	
	return (
		<Row>
		<Col className="right-align white-text col-6 mt-5 p-4"><hr/>
			<Table>
				<thead className="gold form-bg">
				<th>Business Address:</th></thead>
				<tbody className="white-text center form-bg">
				<tr>123 Main St,</tr> 
				<tr>Antipolo City </tr>
				<tr>Philippines </tr>
				<tr>1800</tr>
				</tbody>
			</Table>
				</Col>
		<Col className="right-align white-text col-6 mt-5 p-4"><hr/>

			<Table>
				<thead className="gold form-bg">
				<th>Contact Us:</th></thead>
				<tbody className="white-text center form-bg">
				<tr>Toll Free: 800-123-1234</tr> 
				<tr>Mobile Number: 0916-123-1234</tr>
				<tr>Email: admin@mail.com</tr>
				<tr>Social Media | Facebook | Instagram | Twitter | LinkedIn</tr>
				</tbody>
			</Table>


		
				</Col>
		</Row>
		
	

	)
}